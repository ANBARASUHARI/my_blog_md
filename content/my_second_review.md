Title: My Second Review
Date: 30-08-2023 12:00
Modified: 30-08-2023 12:10
Category: Review
Tags: pelican, publishing
Slug: my-super-post
Authors: Alexis Metaireau, Conan Doyle
Summary: Short version for index and feeds

This is the content of my super blog post.
Please don't skip this 1-minute read. I'm sorry to interrupt, but our fundraiser won’t last long. This Tuesday, 29 August, I humbly request you to reflect on the number of times you visited Wikipedia in the past year, the value you got from it, and whether you're able to contribute ₹ 25. If you can, kindly join the 2% of readers who donate. If everyone reading this right now gave ₹ 25, we'd reach our goal in a couple of hours Second Review.